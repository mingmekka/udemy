import { Component, EventEmitter, Output } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  @Output() selectedMenuItem = new EventEmitter<string>()
  menuItems: MenuItem[];

  ngOnInit() {
    this.menuItems = [{
        label: 'Recipes',
        command: () => {
          this.onSelect('recipe')
        }
    },
    {
        label: 'Shopping List',
        command: () => {
          this.onSelect('shopping')
        }
    },
    {
        label: 'Manage',
        items: [
            {label: 'Save Data'},
            {label: 'Fetch Data'}
        ]
    }
];
  }

  onSelect(selectedMenuItem: string){
    this.selectedMenuItem.emit(selectedMenuItem)
  }
}
