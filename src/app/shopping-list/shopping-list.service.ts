import { EventEmitter } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
export class ShoppingListService {
  ingredientsChanged = new EventEmitter<Ingredient[]>();

  private ingredients: Ingredient[] = [
    new Ingredient('Apples', 5),
    new Ingredient('Banana', 10),
  ];

  getIngridients() {
    return this.ingredients.slice();
  }

  addIngridient(textInput: Ingredient) {
    this.ingredients.push(textInput);
    this.ingredientsChanged.emit(this.ingredients.slice())
  }
}
