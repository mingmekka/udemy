import { Component, Input, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { Ingredient } from 'src/app/shared/ingredient.model';
import { ShoppingListService } from 'src/app/shopping-list/shopping-list.service';
import { Recipe } from '../recipe.model';

interface buttonItem {
  name: string;
  code: string;
}

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css'],
})
export class RecipeDetailComponent implements OnInit {
  dropDownItems: MenuItem[];

  selectedItem: buttonItem;

  @Input() currentRecipeElement: Recipe;

  constructor(private shoppingService: ShoppingListService) {}

  ngOnInit(): void {
    this.dropDownItems = [{ label: 'Edit Recipe' }, { label: 'Delete Recipe' }];
  }

  addIngridientsToShoppingList(ingrdidients: Ingredient[]) {
    for (let ingredient of ingrdidients) {
      this.shoppingService.addIngridient(ingredient);
    }
  }
}
