import { EventEmitter } from '@angular/core';
import { Recipe } from './recipe.model';

export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>();

  currentRecipe: Recipe;

  private recipes: Recipe[] = [
    new Recipe(
      'Mac And Cheese',
      'Tasty MacNcheese Stuff',
      'https://cdn.gutekueche.de/upload/rezept/6822/1600x1200_amerikanische-mac-and-cheese.jpg',
      [
        { name: 'Pasta', amount: 5 },
        { name: 'Cheese', amount: 3 },
      ]
    ),
    new Recipe(
      'Pizza',
      'Tasty Pizza Stuff',
      'https://www.lieferando.de/foodwiki/uploads/sites/8/2018/01/pizza-3.jpg',
      [
        { name: 'Mehl', amount: 1 },
        { name: 'Kaese', amount: 10 },
      ]
    ),
    new Recipe(
      'Shashlik',
      'Tasty Shashlik Stuff',
      'https://upload.wikimedia.org/wikipedia/commons/thumb/8/84/Shashlyk_apr2011.JPG/2560px-Shashlyk_apr2011.JPG',
      [
        { name: 'Meat', amount: 5 },
        { name: 'Zwiebel', amount: 2 },
      ]
    ),
  ];

  getRecipes() {
    return this.recipes.slice();
  }

  setCurrentRecipe(recepe: Recipe) {
    this.currentRecipe = recepe;
  }

  getCurrentRedipe() {
    return this.currentRecipe;
  }
}
